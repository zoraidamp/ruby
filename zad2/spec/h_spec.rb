require 'simplecov'
SimpleCov.start
require_relative '../lib/h'

describe Letter do
  it "is initialized with specified number of arguments" do
    expect { Letter.new() }.not_to raise_error
	expect { Letter.new("arg1") }.not_to raise_error
	expect { Letter.new("arg1", "arg2") }.not_to raise_error
	expect { Letter.new("arg1", "arg2", "arg3") }.not_to raise_error
	expect { Letter.new("arg1", "arg2", "arg3", "arg4") }.not_to raise_error
	expect { Letter.new("arg1", "arg2", "arg3", "arg4", "arg5") }.to raise_error
  end
 
  it "creates object Letter with specified arguments" do
    arg1 = "arg1"
    arg2 = "arg2"
    arg3 = "arg3"
    arg4 = "arg4"
    x = Letter.new("arg1", "arg2", "arg3", "arg4")
    expect(x.znak).to eq(arg1)
    expect(x.ile).to eq(arg2)
    expect(x.left).to eq(arg3)
    expect(x.right).to eq(arg4)
  end 
end


describe "extractMin" do
  $total = 0
  $start = nil  
  a = Letter.new("a", 1)
  a.add()
  b = Letter.new("b", 4)
  b.add()
  c = Letter.new("c", 2)
  c.add()
  e = Letter.new("e", 3)
  e.add()

  it "is defined" do
    #queue: a(1)->b(4)->c(2)->e(3)
    #min: a, 1
    expect { extractMin() }.not_to raise_error
  end
	
  it "find min and delete from queue-when min is in the middle of the queue" do
    #queue: b(4)->c(2)->e(3)
    #min: c, 2
    expect(search("c")).not_to be_nil
    expect(extractMin.znak).to eq("c")
	expect(search("c")).to be_nil
  end
  
  it "find min and delete from queue-when min is the last element in the queue" do
    #queue: b(4)->e(3)
    #min: e, 3
    expect(search("e")).not_to be_nil
    expect(extractMin.znak).to eq("e")
	expect(search("e")).to be_nil
  end
  
  it "find min and delete from queue-when min is the first (and last) element in the queue" do
    #queue: e(3)
    #min: b, 4
    expect(search("b")).not_to be_nil
    expect(extractMin.znak).to eq("b")
	expect(search("b")).to be_nil
  end

end


describe "read" do
  it "open" do
    tekst = Array.new()
	expect { read(tekst) }.not_to raise_error
  end

  it "takes from file InputEasy.txt text and put in the tab" do
    #file contains: abbcdeee
    tekst = Array.new()
    read(tekst)
    output = ["a","b","b","c","d","e","e","e"]
    expect(tekst).to eq(output)
  end
end

describe "add" do
  it "is defined" do
    pom = Letter.new("pom")
    expect { pom.add() }.not_to raise_error
  end

  it "adds element to the end of queue" do
    $total = 0
    $start = nil  
    x = Letter.new("x")
    x.add()
    y = Letter.new("y")
    y.add()
    expect(x.znak).to eq($start.znak)
  end
  
end

describe "search" do
  it "is defined" do
    expect { search("w") }.not_to raise_error
  end

  it "returns nil when there is no element in the queue" do
    expect(search("b")).to be_nil
  end
  
  it "returns object when there is the element in the queue" do
	x = Letter.new("x")
    x.add()
    expect(search("x").znak).to eq("x")
  end
  
end

describe "licz" do
  
  it "is defined" do
    tekst = ["a","b","b","c","d","e","e","e"]
    expect { licz(tekst) }.not_to raise_error
  end
  
  it "inserts into queue elements with chars from tab tekst and there are in the table" do
    $total = 0
    $start = nil  
    tekst = ["a","b","b","c","d","e","e","e"]
    licz(tekst)
    pom = $start
    expect(pom.znak).to eq("a")
    expect(pom.ile).to eq(1)
    pom = pom.nextL
    expect(pom.znak).to eq("b")
    expect(pom.ile).to eq(2)
    pom = pom.nextL
    expect(pom.znak).to eq("c")
    expect(pom.ile).to eq(1)
    pom = pom.nextL
    expect(pom.znak).to eq("d")
    expect(pom.ile).to eq(1)
    pom = pom.nextL
    expect(pom.znak).to eq("e")
    expect(pom.ile).to eq(3)
  end

end

describe "makeTree" do
  it "is defined" do
    $total = 0
    $start = nil  
    a = Letter.new("a", 1)
    a.add()
    b = Letter.new("b", 4)
    b.add()
    expect { makeTree() }.not_to raise_error
  end
  
  it "makes huffman tree based on queue" do
  #structure:expected result
  #         ~
  #     /      \
  #    ~        ~
  #  /  \     /   \
  # d    b   ~     e
  #         /\
  #        a  c
  
    $total = 0
    $start = nil  
    a = Letter.new("a", 1)
    b = Letter.new("b", 2)
    c = Letter.new("c", 1)
    d = Letter.new("d", 1)
    e = Letter.new("e", 3)
    a.add()
    b.add()
    c.add()
    d.add()
    e.add()
    makeTree() 
    expect($root.right.left.left.znak).to eq("a")
    expect($root.left.right.znak).to eq("b")
    expect($root.right.left.right.znak).to eq("c")
    expect($root.left.left.znak).to eq("d")
    expect($root.right.right.znak).to eq("e")
  end 
  
end

describe "inorder" do

  it "is defined" do
    $total = 0
    $start = nil  
    a = Letter.new("a", 1)
    b = Letter.new("b", 4)
    c = Letter.new("c", 4, a, b)  
    $root=c   
    expect { inorder($root, [], 0) }.not_to raise_error
  end  
  
  it "gives chars new shorter code which depend on the tree" do
  #there is
  #         ~
  #     /      \
  #    ~        ~
  #  /  \     /   \
  # d    b   ~     e
  #         /\
  #        a  c
  
    $total = 0
    $start = nil  
    a = Letter.new("a", 1)
    b = Letter.new("b", 4)
    c = Letter.new("c", 4) 
    d = Letter.new("d", 1)
    e = Letter.new("e", 3) 
    zz = Letter.new("~", 3, a, c)   
    z = Letter.new("~", 3, zz, e)
    y = Letter.new("~", 3, d, b)   
    x = Letter.new("~", 3, y, z)  
    a.add()
    b.add()
    c.add()
    d.add()
    e.add()
    $root=x   
    expect { inorder($root, [], 0) }.not_to raise_error
    expect(search("a").kod).to eq([1,0,0])
    expect(search("b").kod).to eq([0,1])
    expect(search("c").kod).to eq([1,0,1])
    expect(search("d").kod).to eq([0,0])
    expect(search("e").kod).to eq([1,1])
  end  

end

describe "kodowanie" do
  it "is defined" do  
    tekst = ["a","b","b","c","d","e","e","e"]
    hcodes = []
    a = Letter.new("a")
    b = Letter.new("b")
    a.kod = [1,0,0]
    b.kod = [0,1]
    a.nast = b
    $first=a     
    expect { kodowanie(tekst, hcodes) }.not_to raise_error
  end 

  it "returns to table hcodes result of coding text" do 
    tekst = ["a","b","b","c","d","e","e","e"]
    hcodes = []
    output = [1,0,0,0,1,0,1,1,0,1,0,0,1,1,1,1,1,1]   
    a = Letter.new("a")
    b = Letter.new("b")
    c = Letter.new("c") 
    d = Letter.new("d")
    e = Letter.new("e")
    a.kod = [1,0,0]
    b.kod = [0,1]
    c.kod = [1,0,1]
    d.kod = [0,0]
    e.kod = [1,1]
    a.nast = b
    b.nast = c
    c.nast = d
    d.nast = e
    $first=a    
    kodowanie(tekst, hcodes)	
    expect(hcodes).to eq(output)
  end   
end

describe "odkodowanie" do
  it "is defined" do  
    hcodes =[1,0,1]
    uncode = []
    a = Letter.new("a", 1)
    b = Letter.new("b", 2)
    c = Letter.new("c", 3, a, b)
    $root=a     
    expect { odkodowanie(uncode, hcodes) }.not_to raise_error
  end 
 
  it "returns to table uncode result of uncoding text" do  
    hcodes =[1,0,0,0,1,0,1,1,0,1,0,0,1,1,1,1,1,1]
    uncode = []
    output = ["a","b","b","c","d","e","e","e"]
    a = Letter.new("a", 1)
    b = Letter.new("b", 4)
    c = Letter.new("c", 4) 
    d = Letter.new("d", 1)
    e = Letter.new("e", 3) 
    zz = Letter.new("~", 3, a, c)   
    z = Letter.new("~", 3, zz, e)
    y = Letter.new("~", 3, d, b)   
    x = Letter.new("~", 3, y, z) 
    $root=x    
    odkodowanie(uncode, hcodes)   
    expect(uncode).to eq(output)
  end 
 
end

describe "printQ" do
  it "is defined" do  
    $total = 0
    $start = nil  
    a = Letter.new("a", 1)
    a.add()
    b = Letter.new("b", 4)
    b.add()
    c = Letter.new("c", 2)
    c.add()
    e = Letter.new("e", 3)
    e.add()  
    expect { printQ() }.not_to raise_error
  end  
 
end

describe "printPomQ" do
  it "is defined" do  
    a = Letter.new("a")
    b = Letter.new("b")
    c = Letter.new("c") 
    a.kod = [1,0,0]
    b.kod = [0,1]
    c.kod = [1,0,1]
    a.nast = b
    b.nast = c
    $first=a 
    expect { printPomQ() }.not_to raise_error
  end  
 
end

describe "printTab" do
  it "is defined" do  
    tab =[1,0,0,1,0,1] 
    expect { printTab(tab) }.not_to raise_error
  end  
 
end